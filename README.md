# Tokenizer
Tokenizer is a microservice for managing ERC20 Tokens.

# Features
* manage a database of compiled contracts
* deploy contracts to the Ethereum mainnet
* manage a database of contract deployemnts
* Inspect balance of a deployed ERC20 contract for a given addres
* Mint tokens of a deployed ERC20 contract to a given address
* Create an Ethereum account


# Installation
## PostgreSQL
Run a container from official PostgreSQL image:
```
docker run --name postgres -e POSTGRES_PASSWORD=ABCabc123 -p 5432:5432 -d postgres
```

You can use
`openssl rand -hex 32` for generating strong secrets like passwords.

Set environment variables specifying the database connection:
```
export POSTGRES_SERVER=localhost
export POSTGRES_USER=postgres
export POSTGRES_PASSWORD=ABCabc123
export POSTGRES_DB=postgres
export ETH_ACCOUNT_PASSWORD=somepassword
```

Place the above lines in a file e.g. dev.env.sh and then source them:
```
source dev.env.sh
```

Look in app/core/config.py for an exhaustive list of environment
variables used to configure the app.

## RabbitMQ
Run a container from official RabbitMQ image:
```
docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 -d rabbitmq:3-management
```

# Running
```
python -m uvicorn app.main:app --reload
```

# Development
## Run migrations
Autogenerate a revision:
```
PYTHONPATH=./ alembic revision --autogenerate -m "Added contracts and deployed_contracts table"
```

Apply migration to the database:
```
PYTHONPATH=./ alembic upgrade head
```

## Run geth in developer mode
```
geth --dev
```

Make sure to set the `ETH_DEV`
environment variable when running with geth in dev mode.
This injects the middleware which fixes the incompatibilities which
geth has in dev mode.
```
export ETH_DEV=1
```
