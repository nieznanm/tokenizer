from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.contract import Contract
from app.models.contract import ContractCreate


def get(db_session: Session, *, id: int) -> Optional[Contract]:
    return db_session.query(Contract).filter(Contract.id == id).first()


def get_multi(db_session: Session, *, skip=0, limit=100) -> List[Optional[Contract]]:
    return db_session.query(Contract).offset(skip).limit(limit).all()


def create(db_session: Session, *, contract_in: ContractCreate) -> Contract:
    contract_in_data = jsonable_encoder(contract_in)
    contract = Contract(**contract_in_data)
    db_session.add(contract)
    db_session.commit()
    db_session.refresh(contract)
    return contract