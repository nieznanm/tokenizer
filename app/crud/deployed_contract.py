from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.db_models.deployed_contract import DeployedContract as DeployedContractDB
from app.models.deployed_contract import DeployedContractCreate


def get(db_session: Session, *, id: int) -> Optional[DeployedContractDB]:
    return (
        db_session.query(DeployedContractDB).filter(DeployedContractDB.id == id).first()
    )


def get_multi(
    db_session: Session, *, skip=0, limit=100
) -> List[Optional[DeployedContractDB]]:
    return db_session.query(DeployedContractDB).offset(skip).limit(limit).all()


def create(
    db_session: Session, *, contract_in: DeployedContractCreate
) -> DeployedContractDB:
    contract_in_data = jsonable_encoder(contract_in)
    contract = DeployedContractDB(**contract_in_data)
    db_session.add(contract)
    db_session.commit()
    db_session.refresh(contract)
    return contract
