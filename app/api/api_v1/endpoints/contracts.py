import json
from typing import List

from fastapi import APIRouter, Depends, HTTPException, File
from sqlalchemy.orm import Session

import app.crud.contract
import app.crud.deployed_contract
from app.api.utils.db import get_db
from app.api.utils import eth
from app.db_models.contract import Contract as DBContract
from app.models.contract import Contract, ContractCreate
from app.models.deployed_contract import DeployedContract, DeployedContractCreate

router = APIRouter()


@router.get("/", response_model=List[Contract])
def read_contracts(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100
):
    """
    Retrieve contracts.
    """

    contracts = app.crud.contract.get_multi(db, skip=skip, limit=limit)
    return contracts


@router.post("/", response_model=Contract)
def create_contract(
    *,
    db: Session = Depends(get_db),
    info_file: bytes = File(...),
    bytecode_file: bytes = File(...)
):
    """
    Create new contract.
    """
    contract_in = ContractCreate(info=json.loads(info_file), bytecode=bytecode_file)
    contract = app.crud.contract.create(db_session=db, contract_in=contract_in)
    return contract


@router.get("/{id}", response_model=Contract)
def read_contract(
    *,
    db: Session = Depends(get_db),
    id: int
):
    """
    Get contract by ID.
    """
    contract = app.crud.contract.get(db_session=db, id=id)
    if not contract:
        raise HTTPException(status_code=400, detail="Contract not found")
    return contract


@router.post("/{id}/deployed_contracts", response_model=DeployedContract)
def create_deployed_contract(
    *,
    db: Session = Depends(get_db),
    id : int
):
    """
    Create new  deployed contract.
    """
    contract = app.crud.contract.get(db_session=db, id=id)
    if not contract:
        raise HTTPException(status_code=400, detail="Contract not found")
    w3 = eth.get_web3()
    address = eth.deploy(w3, contract.info["output"]["abi"], contract.bytecode)
    contract_in = DeployedContractCreate(contract_id=id, address=address)
    deployed_contract = app.crud.deployed_contract.create(db_session=db, contract_in=contract_in)
    print(deployed_contract.contract_id, deployed_contract)
    return deployed_contract
