from typing import List

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud
from app.api.utils.db import get_db
from app.db_models.contract import Contract as DBContract
from app.models.deployed_contract import DeployedContractDeploy, DeployedContract
from app.models.call_parameters import CallParameters

from app.api.utils import eth

router = APIRouter()


@router.get("/", response_model=List[DeployedContract])
def read_deployed_contracts(
    db: Session = Depends(get_db),
    skip: int = 0,
    limit: int = 100
):
    """
    Retrieve deployed contracts.
    """
    contracts = crud.deployed_contract.get_multi(db, skip=skip, limit=limit)
    return contracts


@router.get("/{id}", response_model=DeployedContract)
def read_deployed_contract(
    *,
    db: Session = Depends(get_db),
    id: int
):
    """
    Get deployed contract by ID.
    """
    contract = crud.deployed_contract.get(db_session=db, id=id)
    if not contract:
        raise HTTPException(status_code=400, detail="DeployedContract not found")
    return contract


@router.post("/{id}/call")
def call_method(
    *,
    db: Session = Depends(get_db),
    id: int,
    params: CallParameters
):
    """
    Call a deployed contract's method.
    If live is true create a transaction on the blockchain and return it's hash
    else return the result of method call.
    """
    deployed_contract = crud.deployed_contract.get(db_session=db, id=id)
    if not deployed_contract:
        raise HTTPException(status_code=400, detail="DeployedContract not found")
    contract = crud.contract.get(db_session=db, id=deployed_contract.contract_id)

    w3 = eth.get_web3()
    result = eth.call_method(w3, contract.info["output"]["abi"], deployed_contract.address, params)

    return result
