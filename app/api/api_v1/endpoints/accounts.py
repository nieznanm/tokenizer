from typing import List

from fastapi import APIRouter, Depends, HTTPException
from app.api.utils import eth
import app.core.config


router = APIRouter()


@router.get("/", response_model=List[str])
def read_accounts():
    """
    Retrieve addresses of available accounts.
    """
    w3 = eth.get_web3()
    return w3.eth.accounts


@router.post("/", response_model=str)
def create_account():
    """
    Create new account.
    """
    w3 = eth.get_web3()
    address = w3.geth.personal.newAccount(app.core.config.ETH_ACCOUNT_PASSWORD)
    return address
