from fastapi import APIRouter

from app.api.api_v1.endpoints import contracts, deployed_contracts, accounts

api_router = APIRouter()
api_router.include_router(contracts.router, prefix="/contracts", tags=["contracts"])
api_router.include_router(
    deployed_contracts.router, prefix="/deployed_contracts", tags=["deployed_contracts"]
)
api_router.include_router(accounts.router, prefix="/accounts", tags=["accounts"])