import web3
import app.core.config
from typing import Dict, List
from app.models.call_parameters import CallParameters


def get_web3():
    w3 = web3.Web3(web3.IPCProvider(app.core.config.ETH_IPC_ENDPOINT))
    if app.core.config.ETH_DEV:
        w3.middleware_onion.inject(web3.middleware.geth_poa_middleware, layer=0)

    return w3


def deploy(w3: web3.Web3, abi: List, bytecode: str):
    w3.geth.personal.unlockAccount(
        app.core.config.ETH_ACTIVE_ADDRESS, app.core.config.ETH_ACCOUNT_PASSWORD
    )
    deploy_transaction = {"from": app.core.config.ETH_ACTIVE_ADDRESS}

    contract = w3.eth.contract(bytecode=bytecode, abi=abi)
    print("ESTIMATE GAS:", contract.constructor().estimateGas(deploy_transaction))
    tx_hash = contract.constructor().transact(deploy_transaction)
    print("tx_hash", tx_hash)
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)
    print("tx_receipt", tx_receipt)
    print(tx_receipt.contractAddress)

    return tx_receipt.contractAddress


def call_method(w3: web3.Web3, abi: List, address: str, params: CallParameters):
    contract = w3.eth.contract(address=address, abi=abi)
    function = contract.get_function_by_name(params.method)
    transaction = {"from": app.core.config.ETH_ACTIVE_ADDRESS}
    print(contract.all_functions())
    if params.live:
        w3.geth.personal.unlockAccount(
            app.core.config.ETH_ACTIVE_ADDRESS, app.core.config.ETH_ACCOUNT_PASSWORD
        )
        return function(*params.args).transact(transaction).hex()
    else:
        return function(*params.args).call(transaction)
