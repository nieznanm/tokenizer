import os

POSTGRES_SERVER = os.getenv("POSTGRES_SERVER")
POSTGRES_USER = os.getenv("POSTGRES_USER")
POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
POSTGRES_DB = os.getenv("POSTGRES_DB")
SQLALCHEMY_DATABASE_URI = (
    f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}/{POSTGRES_DB}"
)

API_V1_STR = "/api/v1"

# The default value for geth dev mode is /tmp/geth.ipc, for mainnet ~/.ethereum/geth.ipc
ETH_IPC_ENDPOINT = os.getenv("ETH_IPC_ENDPOINT", "~/.ethereum/geth.ipc")
ETH_ACTIVE_ADDRESS = os.getenv("ETH_ACTIVE_ADDRESS")
ETH_ACCOUNT_PASSWORD = os.getenv("ETH_ACCOUNT_PASSWORD")
ETH_DEV = os.getenv("ETH_DEV", False)