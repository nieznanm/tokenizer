from sqlalchemy import JSON, Boolean, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class DeployedContract(Base):
    __tablename__ = "deployed_contracts"
    id = Column(Integer, primary_key=True, index=True)
    address = Column(String)
    contract_id = Column(Integer, ForeignKey("contracts.id"))
