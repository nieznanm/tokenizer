from sqlalchemy import Boolean, Column, Integer, String, JSON
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Contract(Base):
    __tablename__ = 'contracts'
    id = Column(Integer, primary_key=True, index=True)
    info = Column(JSON)
    bytecode = Column(String)
