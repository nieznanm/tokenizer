from .common import BaseModel
from typing import List

class CallParameters(BaseModel):
    method: str
    args: List
    live: bool