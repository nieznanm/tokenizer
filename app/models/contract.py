from typing import Dict
from .common import BaseModel


class ContractBase(BaseModel):
    info: Dict = None
    bytecode: str = None


class ContractCreate(ContractBase):
    pass


class Contract(ContractBase):
    id: int = None
