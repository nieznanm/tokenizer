from .common import BaseModel


class DeployedContractBase(BaseModel):
    contract_id: int = None


class DeployedContractDeploy(DeployedContractBase):
    pass


class DeployedContractCreate(DeployedContractBase):
    address: str = None


class DeployedContract(DeployedContractBase):
    id: int = None
    address: str = None
